"use strict";
const express = require("express");
const bodyParser = require("body-parser");
const http = require('http');
const API_KEY = require('./apiKey');
const app = express();
// const dialogflow = require('dialogflow');
var request = require('request');
const crypto = require('crypto');
const uuid = require('uuid');
// const WebhookClient = require('dialogflow-fulfillment').WebhookClient;
const { WebhookClient } = require('dialogflow-fulfillment');
const { Text, Card, Image, Suggestion, Payload } = require('dialogflow-fulfillment');
// const { Card, Suggestion } = require('dialogflow-fulfillment');
// const DialogflowApp = require('actions-on-google')
const DialogflowApp = require('actions-on-google').DialogflowApp;
// const { dialogflow } = require('actions-on-google')
// const intents = require('./intents')
const { dialogflow, Permission } = require('actions-on-google');
const googleAssistantRequest = 'google';
// const {dialogflow, Suggestions, SimpleResponse, Permission} = require('actions-on-google')  
// const functions = require('firebase-functions'); 
// const geocoder = require('geocoder');
// const dialogflowApp = dialogflow({ debug: true });
// const welcome = 'Default Welcome Assistant';
const { Carousel, List, BrowseCarousel, BrowseCarouselItem } = require('actions-on-google');
const port = process.env.PORT || 8000;
// Instantiate a new API.AI assistant object.
const assistant = dialogflow();

// app.post('/', function (request, response) {
//   const agent = new WebhookClient({
//     request: req,
//     response: res
//   });
// })

// const PLATFORMS = {
//   UNSPECIFIED: 'PLATFORM_UNSPECIFIED',
//   FACEBOOK: 'FACEBOOK',
//   SLACK: 'SLACK',
//   TELEGRAM: 'TELEGRAM',
//   KIK: 'KIK',
//   SKYPE: 'SKYPE',
//   LINE: 'LINE',
//   VIBER: 'VIBER',
//   ACTIONS_ON_GOOGLE: 'ACTIONS_ON_GOOGLE',
// };

// const SUPPORTED_RICH_MESSAGE_PLATFORMS = [
//   PLATFORMS.FACEBOOK,
//   PLATFORMS.SLACK,
//   PLATFORMS.TELEGRAM,
//   PLATFORMS.KIK,
//   PLATFORMS.SKYPE,
//   PLATFORMS.LINE,
//   PLATFORMS.VIBER,
//   PLATFORMS.ACTIONS_ON_GOOGLE,
// ];




app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.use(bodyParser.json());


// "body-parser": "^1.15.0",
// "express": "^4.13.4"

app.use((req, res, next)=> {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, OPTIONS");
    next();
})


app.get('/', function (req, res) {
    // const agent = new WebhookClient({request: req, response: res});
    res.send('Hello, I am a storebotV2')
})

// Handle AoG assistant intent
// assistant.intent('Default Welcome Assistant', conv => {
//   console.log('intent is triggered');
 
// });

function getData() {
  console.log("Get Data triggered");
}



app.post('/webhook', (req, res, next) => {
// console.log("actions 3", req.body.queryResult);
  const agent = new WebhookClient({request: req, response: res});
  // console.log("actions 4", req.body.queryResult.intent.displayName);
  let actionName = req.body.queryResult.action;
  let intentName = req.body.queryResult.intent.displayName;
  console.log("display Name", actionName)
   
  // console.log('Dialogflow Request headers: ' + JSON.stringify(req.headers));
  // console.log('Dialogflow Request body: ' + JSON.stringify(req.body));


  function welcome(agent) {
    agent.add(`Hello! What can I do for you today?`);
    agent.add(new Suggestion(`Product Search`));
    agent.add(new Suggestion(`Order Status`));
    agent.add(new Suggestion(`Store Locator`));
  }

  function welcomeGreet(agent) { 
    agent.add(`Good, thank You. How can I help you today?`);
    agent.add(new Suggestion(`Product Search`));
    agent.add(new Suggestion(`Order Status`));
    agent.add(new Suggestion(`Store Locator`));
  }

  function fallback(agent) {
    // agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);

    let queryText = req.body.queryResult.queryText;
    let actionText = req.body.queryResult.action;
    console.log("saving text is", queryText, actionText);
    const SaveFallbackApiUrl = "http://himalayaforu.com/chatbot/actions/status";
    var headers = {
      'Authorization': 'Basic U3Jpbml2YXM6S2FsYXBhbGE=',
      'Content-Type': 'application/json'
    };

    var saveData = {
      Action: actionText,
      InputText: queryText
    }
    console.log("saveData", saveData);

    var fallbackoptions = {
      url: SaveFallbackApiUrl,
      method: 'POST',
      form: saveData,
      headers: headers
    }

    request(fallbackoptions,  function (error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log("save fallback",JSON.parse(body), body)
          console.log("saved",body);
        }
    });




    // agent.add(`Sorry I didn't get your question`);
  }


  function other(agent) {
    agent.add(`This message is from Dialogflow's Cloud Functions for Firebase editor!`);
    agent.add(new Card({
        title: `Title: this is a card title`,
        imageUrl: `https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png`,
        text: `This is the body text of a card.  You can even use line\n  breaks and emoji! 💁`,
        buttonText: 'This is a button',
        buttonUrl: `https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png`
      })
    );
    agent.add(new Suggestion(`Quick Reply`));
    agent.add(new Suggestion(`Suggestion`));
    agent.context.set({ name: 'weather', lifespan: 2, parameters: { city: 'Rome' }});
    //setContext is deprecated, migrate to `context.set`
  }


  function googleAssistantOther(agent) {
    // let conv = agent.conv();
    //     conv.ask("HEY");
    //     return agent.add(conv);
    if(agent.requestSource === agent.ACTIONS_ON_GOOGLE) {

    

      const conv = agent.conv(); // Get Actions on Google library conversation object
      if (!conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT')) {
        conv.ask('Sorry, try this on a screen device or select the ' +
          'phone surface in the simulator.');
        return;
      }


      conv.ask('This is a list example.');
      // Create a list
      conv.ask(new List({
        title: 'List Title',
        items: {
          // Add the first item to the list
          'SELECTION_KEY_ONE': {
            synonyms: [
              'synonym 1',
              'synonym 2',
              'synonym 3',
            ],
            title: "GEL",
            description: "Cardamom - Clove - Fennel - Indian Dill - Menthol - Miswak",
            image: {
              url: "https://www.himalayawellness.in/-/media/Images/Habitat/personal-care/382-397/active-fresh-gel.png",
              accessibilityText: "Active Fresh"
            },
          },
          // Add the second item to the list
          'SELECTION_KEY_GOOGLE_HOME': {
            synonyms: [
              'Google Home Assistant',
              'Assistant on the Google Home',
          ],
          title: "Active fresh",
          description: "himalaya-active-fresh-mouthwash.",
          image: {
            url: "https://www.himalayawellness.in/-/media/Images/Habitat/personal-care/382-397/himalaya-active-fresh-mouthwash.png",
            accessibilityText: "Active fresh"
            },
          },
          // Add the third item to the list
          'SELECTION_KEY_GOOGLE_PIXEL': {
            synonyms: [
              'Google Pixel XL',
              'Pixel',
              'Pixel XL',
            ],
            title: "Repairs and protects for youthful hands.",
            description: "Cocoa butter - Greater Galangal - Rosemyrtle - Spiked Ginger Lily - Woodfordia",
            image: {
              url: "https://www.himalayawellness.in/-/media/Images/Habitat/personal-care/382-397/age-defying-hand-cream.png",
              accessibilityText: "Age Defying Hand Cream"
            }
          },
        },
      }));
      return agent.add(conv); 
    }

    //   let text = 'Please choose an item:';
    //   conv.ask(text); 
    //   // conv.close(text); // Use Actions on Google library to add responses
    //   conv.ask(new Carousel({
    //     title: 'Google Assistant',
    //     items: {
    //       'WorksWithGoogleAssistantItemKey': {
    //         title: 'Works With the Google Assistant',
    //         description: 'If you see this logo, you know it will work with the Google Assistant.',
    //         image: {
    //           url: `https://developers.google.com/actions/images/badges/XPM_BADGING_GoogleAssistant_VER.png`,
    //           accessibilityText: 'Works With the Google Assistant logo',
    //         },
    //       },
    //       'GoogleHomeItemKey': {
    //         title: 'Google Home',
    //         description: 'Google Home is a powerful speaker and voice Assistant.',
    //         image: {
    //           url: `https://lh3.googleusercontent.com/Nu3a6F80WfixUqf_ec_vgXy_c0-0r4VLJRXjVFF_X_CIilEu8B9fT35qyTEj_PEsKw`,
    //           accessibilityText: 'Google Home'
    //         },
    //       },
    //     },
    //   }));
    //   // Add Actions on Google library responses to your agent's response
    //    return agent.add(conv);
    // }

    if(agent.requestSource === agent.FACEBOOK) {
      console.log("Facebook response");
      const facebookPayload = {
        attachment: {
          type: 'template',
          payload: {
            template_type: 'generic',
            elements: [
              {
                title: 'H Logo',
                image_url: `https://www.himalayawellness.in/-/media/Project/Himalaya/Himalaya-Wellness/Banner/About-himalaya.jpg?h=330&la=en&w=349&hash=42FD7177B95FE449C148F77B9C887ABB`,
                subtitle: 'Elephant packshot',
                default_action: {
                  type: 'web_url',
                  url: 'https://github.com/fluidicon.png',
                },
                buttons: [
                  {
                    type: 'web_url',
                    url: 'https://github.com/fluidicon.png',
                    title: 'This is a button',
                  },
                ],
              },
              {
                title: 'H Logo',
                image_url: `https://www.himalayawellness.in/-/media/Project/Himalaya/Himalaya-Wellness/Banner/About-himalaya.jpg?h=330&la=en&w=349&hash=42FD7177B95FE449C148F77B9C887ABB`,
                subtitle: 'Elephant packshot',
                default_action: {
                  type: 'web_url',
                  url: 'https://github.com/fluidicon.png',
                },
                buttons: [
                  {
                    type: 'web_url',
                    url: 'https://github.com/fluidicon.png',
                    title: 'This is a button',
                  },
                ],
              },
              {
                title: 'H Logo',
                image_url: `https://www.himalayawellness.in/-/media/Project/Himalaya/Himalaya-Wellness/Banner/About-himalaya.jpg?h=330&la=en&w=349&hash=42FD7177B95FE449C148F77B9C887ABB`,
                subtitle: 'Elephant packshot',
                default_action: {
                  type: 'web_url',
                  url: 'https://github.com/fluidicon.png',
                },
                buttons: [
                  {
                    type: 'web_url',
                    url: 'https://github.com/fluidicon.png',
                    title: 'This is a button',
                  },
                ],
              },
            ],
          },
        },
      };
      let payload = new Payload(agent.FACEBOOK, facebookPayload)

      return agent.add(payload);
      
    }

  }

  // function orderstatus(agent) {
    // return OrderStatusApi()
    //   .then(data => {
    //       agent.add(data);
    //     })
    //   .catch(error => {
    //     agent.add("Order Not Found. Please enter correct order and mobile Number");
    // })



  // }

  function orderstatus(agent) {
    console.log("order status is");
    let phoneNum = req.body.queryResult.parameters['phone-number'];
    let orderNum = req.body.queryResult.parameters['order-number'];

    console.log("phone number 1 is", phoneNum);
    console.log("order number 1 is", orderNum);

    const orderstatusApiUrl = "http://himalayaforu.com/chatbot/orders/status";
    var headers = {
      'Authorization': 'Basic U3Jpbml2YXM6S2FsYXBhbGE=',
      'Content-Type': 'application/json'
    };

    var orderStatusData = {
      OrderNumber: orderNum,
      mobileno: phoneNum
    }

    console.log("First", orderStatusData);
    var trackOption = {
      url: orderstatusApiUrl,
      method: 'POST',
      form: orderStatusData,
      headers: headers
    }

    var orderstatusData = [];
    var orderitemData = [];
    let orderStatusoutput;


    return new Promise(function(resolve, reject)  {
      request(trackOption,  function (error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log("Order Status Data",JSON.parse(body), body)
           let bodyData = JSON.parse(body);
           let card;
           if (bodyData.length>=1) {
             for (var val in bodyData) {
                console.log("order status", bodyData[val].status);
                if (bodyData[val].trackingURL != '') {
                  card = {
                    title: 'Your Order Status is '+ bodyData[val].status +'\n\n',
                    imageUrl: 'https://ak0.picdn.net/shutterstock/videos/17208160/thumb/12.jpg',
                    text: 'Courier Name : '+bodyData[val].courierName+'\n\n Order Amount : ₹ '+bodyData[val].OrderAmount+'\n Airway Bill Number : '+bodyData[val].airway_billno+'', 
                    buttonText: 'Track your order',
                    buttonUrl: ''+bodyData[val].trackingURL+''
                  }
                } else {
                  card = {
                    title: 'Your Order Status is '+ bodyData[val].status +'\n\n',
                    imageUrl: 'https://ak0.picdn.net/shutterstock/videos/17208160/thumb/12.jpg',
                    text: 'Courier Name : '+bodyData[val].courierName+'\n\n Order Amount : ₹ '+bodyData[val].OrderAmount+'\n Airway Bill Number : '+bodyData[val].airway_billno+'', 
                    buttonText: 'Track your order',
                    buttonUrl: 'https://himalayawellness.in'
                  }
                }
              }
              orderStatusoutput = agent.add('Your Order Status is '+ bodyData[val].status+'\n')
              orderStatusoutput += agent.add(new Card(card)); 
              resolve(orderStatusoutput) 
            }
            else {
              console.log("Data not Found");
              orderStatusoutput = agent.add(
                new Image('https://user-images.githubusercontent.com/24848110/33519396-7e56363c-d79d-11e7-969b-09782f5ccbab.png')
              );
              orderStatusoutput += agent.add("\n Please enter correct details");
              resolve(orderStatusoutput)
            }
           
          }
        });
    });
    // return new Promise((resolve, reject) => {
    //   request(trackOption,  function (error, response, body) {
    //     if (!error && response.statusCode == 200) {
    //         console.log("Order Status Data",JSON.parse(body), body)
    //         let bodyData = JSON.parse(body);
    //         let card;
    //         for (var val in bodyData) {
    //           console.log("order status", bodyData[val].status);
    //           card = {
    //             title: 'Your Order Status is '+ bodyData[val].status +'',
    //             imageUrl: 'https://ak0.picdn.net/shutterstock/videos/17208160/thumb/12.jpg',
    //             text: 'Courier Name : '+bodyData[val].courierName+' Order Amount : Rs'+bodyData[val].OrderAmount+' Airway Bill Number : '+bodyData[val].airway_billno+'', 
    //             buttonText: 'Track your order',
    //             buttonUrl: bodyData[val].trackingURL
    //           }
    //         }
    //         agent.add(new Card(card));             
    //         resolve()
    //     } else {

    //     }
    //   })
    // })

  }

  function storelocator(agent) {
    console.log("store locator is")
    // agent.add("Fetching stores....")
    let state = req.body.queryResult.parameters['state'];
    console.log("store locator 1 is", state);

    const stateApiUrl = `http://api.himalayawellness.in/HstoreAPI/StoreLocator/cities/english/${state}`;
    var headers = {
      'Authorization': 'Basic U3Jpbml2YXM6S2FsYXBhbGE=',
      'Content-Type': 'application/json'
    };

    var stateOption = {
      url: stateApiUrl,
      method: 'GET',
      headers: headers
    }

    // var orderstatusData = [];
    // var orderitemData = [];
    //commit
    let orderStatusoutput;


    return new Promise(function(resolve, reject)  {
      request(stateOption,  function (error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log("store locator Data",JSON.parse(body))
          let bodyData = JSON.parse(body);
          let statechip;
          if (bodyData.length>1)
          {
            console.log("Data Exists");
            statechip = agent.add("Please select a city");
            for(var val in bodyData) {
              statechip += agent.add(new Suggestion(bodyData[val]));
              console.log("statechip", bodyData[val])
            }
            resolve(statechip);
          }
          else {
            statechip = agent.add("Please enter your state");
            resolve(statechip);
            console.log("No Data Found");
          }
           
          }
        });
    });  
  }
  
  function storelocatorcity(agent) {
    console.log("storelocatorcity");
    let state = req.body.queryResult.parameters['state'];
    let city = req.body.queryResult.parameters['city'];
    console.log("state and city is", state, city);
    // let state = req.body.queryResult.parameters['state'];
    console.log("store locator 1 is", state);

    const statecityApiUrl = `http://api.himalayawellness.in/HstoreAPI/StoreLocator/stores/english/${state}/${city}`;
   
    var headers = {
      'Authorization': 'Basic U3Jpbml2YXM6S2FsYXBhbGE=',
      'Content-Type': 'application/json'
    };

    var statecityOption = {
      url: statecityApiUrl,
      method: 'GET',
      headers: headers
    }

    let statusCityoutput;
    var arrData = [];
    var itemData = [];
    var storelementsVal = [];

    return new Promise(function(resolve, reject)  {
      request(statecityOption,  function (error, response, body) {
        if (!error && response.statusCode == 200) {
          console.log("store locator Data",JSON.parse(body))
          let bodyData = JSON.parse(body);
          let storesList;
          let storeoutput;
          let googlePayload;
          let facebookPayload;

          if (bodyData.length != 0)
          {
            console.log("Data Exists");
            if(agent.requestSource === agent.ACTIONS_ON_GOOGLE) { 
              console.log("ACTIONS_ON_GOOGLE response !");
              if (bodyData.length <= 1) {
                console.log("yes its 1");
                for (var val in bodyData) {
                  storelementsVal.push({
                    simpleResponse: {
                      textToSpeech: "Please find your store below"
                    }
                    },
                    { 
                      basicCard: {
                        title: bodyData[val].store_name+'\n',
                        subtitle: '\nAddress : '+bodyData[val].store_add1+'\n '+bodyData[val].store_add2+'\n '+bodyData[val].store_city+'\n '+bodyData[val].store_state+'\n '+bodyData[val].store_pcode+'\n '+bodyData[val].store_phone+'\n '+bodyData[val].store_email+'',
                        formattedText: bodyData[val].store_name+'\n',
                        image: {
                          url: 'http://himalayaforu.com/Appimages/logo/logo.png',
                          accessibilityText:  bodyData[val].store_name+'\n'
                        },
                        buttons: [
                          {
                            title: "Go to Google Map",
                            openUrlAction: {
                              url: 'https://himalayawellness.in/'
                            }
                          }
                        ],
                        imageDisplayOptions: "CROPPED"
                      }
                    }
                  )
                }
                googlePayload = {
                  expectUserResponse: true,
                  richResponse: {
                      items: storelementsVal
                    }
                };
              }
              else {
                console.log("no its more than 1");
                for (var val in bodyData) {
                  storelementsVal.push({
                    title: bodyData[val].store_name+'\n',
                    openUrlAction: {
                      url: 'https://himalayawellness.in/'
                    },
                    description: '\nAddress : '+bodyData[val].store_add1+'\n '+bodyData[val].store_add2+'\n '+bodyData[val].store_city+'\n '+bodyData[val].store_state+'\n '+bodyData[val].store_pcode+'\n '+bodyData[val].store_phone+'\n '+bodyData[val].store_email+'',
                    footer: bodyData[val].store_name+'\n',
                    image: {
                      url: 'http://himalayaforu.com/Appimages/logo/H.png',
                      accessibilityText: bodyData[val].store_name+'\n'
                    }
                  })
                }

                googlePayload = {
                  expectUserResponse: true,
                  richResponse: {
                    items: [
                      {
                        simpleResponse: {
                          textToSpeech: "Please find your store below"
                        }
                      },
                      {
                        carouselBrowse: {
                          items: storelementsVal
                       },
                      }
                    ],
                   }
                };

              }

              let gpayload = new Payload(agent.ACTIONS_ON_GOOGLE, googlePayload)
              // console.log("gpayload", gpayload);
              storesList = agent.add(gpayload);
            }
            // storesList = agent.add("Please find your store below");
            // for(var val in bodyData) {

            //   if(agent.requestSource === agent.ACTIONS_ON_GOOGLE) { 
                
            //   }

            //   storesList += agent.add(new Card({
            //     title: bodyData[val].store_name+'\n',
            //     imageUrl: 'https://lh5.googleusercontent.com/p/AF1QipOLR7hOcupnGM-UYAREeD8-DyUpoUeZmA6hrPkN=s435-k-no',
            //     text: '\nAddress : '+bodyData[val].store_add1+'\n '+bodyData[val].store_add2+'\n '+bodyData[val].store_city+'\n '+bodyData[val].store_state+'\n '+bodyData[val].store_pcode+'\n '+bodyData[val].store_phone+'\n '+bodyData[val].store_email+'', 
            //     buttonText: 'Go to Google Map',
            //     buttonUrl: 'https://lh5.googleusercontent.com/p/AF1QipOLR7hOcupnGM-UYAREeD8-DyUpoUeZmA6hrPkN=s435-k-no'
            //   }));




            //   console.log("storesList", bodyData[val].store_pcode)
            // }
            if (agent.requestSource === agent.FACEBOOK) {
              console.log("Facebook response !");
              if (bodyData.length <= 1) {
                console.log("yes its 1");
                for (var val in bodyData) {
                  storelementsVal.push({
                    title: bodyData[val].store_name+'\n',
                    image_url: 'https://logonoid.com/images/himalaya-logo.jpg',
                    subtitle: '\nAddress : '+bodyData[val].store_add1+'\n '+bodyData[val].store_add2+'\n '+bodyData[val].store_city+'\n '+bodyData[val].store_state+'\n '+bodyData[val].store_pcode+'\n '+bodyData[val].store_phone+'\n '+bodyData[val].store_email+'',
                    default_action: {
                      type: "web_url",
                      url: 'https://himalayawellness.in/',
                      webview_height_ratio: "COMPACT"
                    },  
                    buttons: [
                      {
                        type:"web_url",
                        url: 'https://himalayawellness.in/',
                        title:"Go to GoogleMap"
                      }
                    ]  
                  })
                }
                facebookPayload = {
                  attachment:{
                    type:"template",
                    payload: {
                      template_type:"generic",
                      elements:  storelementsVal
                    }
                  }
                }


              } else {
                console.log("no its more than 1");
                for (var val in bodyData) {
                  console.log("condition Name is", bodyData[val].store_name+'\n');
                  storelementsVal.push({
                    title: bodyData[val].store_name+'\n',
                    image_url: 'https://logonoid.com/images/himalaya-logo.jpg',
                    subtitle: '\nAddress : '+bodyData[val].store_add1+'\n '+bodyData[val].store_add2+'\n '+bodyData[val].store_city+'\n '+bodyData[val].store_state+'\n '+bodyData[val].store_pcode+'\n '+bodyData[val].store_phone+'\n '+bodyData[val].store_email+'',
                    default_action: {
                      type: 'web_url',
                      url: 'https://himalayawellness.in/',
                    },
                    buttons: [
                      {
                        type: 'web_url',
                        url: 'https://himalayawellness.in/',
                        title: bodyData[val].store_name+'\n',
                      },
                    ]
                  })
                }

                facebookPayload = {
                  attachment: {
                    type: 'template',
                    payload: {
                      template_type: 'list',
                      top_element_style: "compact",
                      elements: storelementsVal
                    },
                  },
                };
              }
              let fbpayload = new Payload(agent.FACEBOOK, facebookPayload)
              storesList = agent.add(fbpayload);
            }
            resolve(storesList);
          }
          else {
            storesList = agent.add("Please enter your city");
            resolve(storesList);
            console.log("No Data Found");
          }
          }
        });
    });  

    // agent.add("state and city is");
  }




  function facebookResponsev2(agent) {
    console.log("facebook response v2")
    agent.add("facebook....");
  }

  function inputUnknown(agent) {
    console.log("inputUnknown v2")
    agent.add("Sorry I didn't get your question");
  }


  function facebookResponse(agent) {
    agent.add(`facebook v2 response`);
    const facebookPayload = {
      attachment: {
        type: 'template',
        payload: {
          template_type: 'generic',
          elements: [
            {
              title: 'H Logo',
              image_url: `https://www.himalayawellness.in/-/media/Project/Himalaya/Himalaya-Wellness/Banner/About-himalaya.jpg?h=330&la=en&w=349&hash=42FD7177B95FE449C148F77B9C887ABB`,
              subtitle: 'Elephant packshot',
              default_action: {
                type: 'web_url',
                url: 'https://github.com/fluidicon.png',
              },
              buttons: [
                {
                  type: 'web_url',
                  url: 'https://github.com/fluidicon.png',
                  title: 'This is a button',
                },
              ],
            },
          ],
        },
      },
    };

    // res.json(responseJson);
    let payload = new Payload(agent.FACEBOOK, facebookPayload)
    return agent.add(payload);
  }

  function productsearch(agent) {
    console.log("product search is1");
    
    return SearchProductApi()
      .then(data => {
        agent.add(data);
      })
      .catch(() => {
        agent.add("limit exceeds or Data Not Found");
    })

  }

   function SearchProductApi() {
    let product = req.body.queryResult.parameters.product;
    // console.log("search product is2", product);
    const SearchProductApiUrl = "http://himalayaforu.com/chatbot/products/SearchByName";
    var headers = {
      'Authorization': 'Basic U3Jpbml2YXM6S2FsYXBhbGE=',
      'Content-Type': 'application/json'
    };

    var searchdata = {
      value: product
    }
    console.log("First", searchdata);
    var options = {
      url: SearchProductApiUrl,
      method: 'POST',
      form: searchdata,
      headers: headers
    }
    

    var arrData = [];
    var itemData = [];

    return new Promise((resolve, reject) => {
      request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log("Promise",JSON.parse(body))

            var elementsVal = [];
            let bodyData = JSON.parse(body);
            let output;
            let facebookPayload;
            let googlePayload;

        if (agent.requestSource === agent.FACEBOOK) {
            console.log("Facebook response !");
            if (bodyData.products.length === 0) {
              // facebookPayload = agent.add(new Text('Product is not available')); 
              // agent.add(new Payload(agent.FACEBOOK, facebookPayload, {sendAsMessage:true}) );
              facebookPayload = { 
                message: {
                  text:"Product is not available"
                }
              }
            }
            else if (bodyData.products.length <= 1) {
              console.log("yes its 1");
              for (var val in bodyData.products) {
                // console.log("Condition Name is", bodyData.products[val].Name);
                elementsVal.push({
                      title: bodyData.products[val].Name+'\n',
                      image_url: bodyData.products[val].imageURL,
                      subtitle:  '\n'+bodyData.products[val].description +'\nVariant:'+ bodyData.products[val].variant +'\n Price: ₹'+ bodyData.products[val].price,
                      default_action: {
                        type: "web_url",
                        url: bodyData.products[val].imageURL,
                        webview_height_ratio: "COMPACT"
                      },
                      buttons: [
                        {
                          type:"web_url",
                          url: bodyData.products[val].imageURL,
                          title:"View Website"
                        },{
                          type:"postback",
                          title:"Buy",
                          payload:"DEVELOPER_DEFINED_PAYLOAD"
                        }  
                      ]  
                  })
              }

              facebookPayload = {
                attachment:{
                  type:"template",
                  payload: {
                    template_type:"generic",
                    elements:  elementsVal
                  }
                }
              }
              // console.log("fce", facebookPayload);
            }
            else {
              console.log("no its more than 1");
              for (var val in bodyData.products) {
                console.log("condition Name is", bodyData.products[val].Name);
                elementsVal.push( {
                  title: bodyData.products[val].Name+'\n',
                  image_url: bodyData.products[val].imageURL,
                  subtitle: '\n'+bodyData.products[val].description +'\n Variant: '+ bodyData.products[val].variant +'\n Price: ₹'+ bodyData.products[val].price,
                  default_action: {
                    type: 'web_url',
                    url: bodyData.products[val].imageURL,
                  },
                  buttons: [
                    {
                      type: 'web_url',
                      url: bodyData.products[val].imageURL,
                      title: bodyData.products[val].Name,
                    },
                  ]
                 })
              }

              facebookPayload = {
                attachment: {
                  type: 'template',
                  payload: {
                    template_type: 'list',
                    top_element_style: "large",
                    elements: elementsVal
                  },
                },
              };
              
            }


            // for (var val in bodyData.products) {
            //   //  console.log("Name is", bodyData.products[val].Name);
            //    elementsVal.push( {
            //     title: bodyData.products[val].Name,
            //     image_url: bodyData.products[val].imageURL,
            //     subtitle: bodyData.products[val].description +'Variant:'+ bodyData.products[val].variant +'Price:'+ bodyData.products[val].price,
            //     default_action: {
            //       type: 'web_url',
            //       url: bodyData.products[val].imageURL,
            //     },
            //     buttons: [
            //       {
            //         type: 'web_url',
            //         url: bodyData.products[val].imageURL,
            //         title: bodyData.products[val].Name,
            //       },
            //     ]
            //    })
            // }
        
            // const facebookPayload = {
            //   attachment: {
            //     type: 'template',
            //     payload: {
            //       template_type: 'list',
            //       top_element_style: "large",
            //       elements: elementsVal
            //     },
            //   },
            // };
        
            let spayload = new Payload(agent.FACEBOOK, facebookPayload)
            // console.log("spayload", spayload);
            output = agent.add(spayload);
          }

           if(agent.requestSource === agent.ACTIONS_ON_GOOGLE) { 
              console.log("ACTIONS_ON_GOOGLE response !");
              if (bodyData.products.length === 0) {
                googlePayload = {
                  expectUserResponse: true,
                  richResponse: {
                      items: [{
                        simpleResponse: {
                          textToSpeech: "Product is not available",
                          displayText: "Product is not available"
                        }
                      }]
                    }
                } 
                // agent.add( new Payload(agent.ACTIONS_ON_GOOGLE, googlePayload, {sendAsMessage:true}) );
  
              }
              else if (bodyData.products.length <= 1) {
                console.log("yes its 1");
                for (var val in bodyData.products) {
                  // console.log("Name is", bodyData.products[val].Name);
                  elementsVal.push({
                      simpleResponse: {
                        textToSpeech: "Please find the product below"
                      }
                      },
                      {
                        basicCard: {
                          title: bodyData.products[val].Name+'\n',
                          subtitle: '\n'+bodyData.products[val].description +'\nVariant:'+ bodyData.products[val].variant +'\n',
                          formattedText:  'Price:  ₹'+ bodyData.products[val].price+'\n',
                          image: {
                            url: bodyData.products[val].imageURL,
                            accessibilityText:  bodyData.products[val].Name
                          },
                          buttons: [
                            {
                              title: "More Info",
                              openUrlAction: {
                                url: bodyData.products[val].imageURL
                              }
                            }
                          ],
                          imageDisplayOptions: "CROPPED"
                        }
                      })
               }

               googlePayload = {
                expectUserResponse: true,
                richResponse: {
                    items: elementsVal
                  }
              };


              }
              else {
                console.log("no its more than 1");
                for (var val in bodyData.products) {
                  // console.log("Name is", bodyData.products[val].Name);
                  elementsVal.push({
                      title: bodyData.products[val].Name+'\n',
                      openUrlAction: {
                        url: bodyData.products[val].imageURL
                      },
                      description: '\n'+bodyData.products[val].description +'\nVariant:'+ bodyData.products[val].variant +'\n',
                      footer: 'Price: ₹'+ bodyData.products[val].price,
                      image: {
                        url: bodyData.products[val].imageURL,
                        accessibilityText: bodyData.products[val].Name
                      }
                  })
               }

                googlePayload = {
                expectUserResponse: true,
                richResponse: {
                  items: [
                    {
                      simpleResponse: {
                        textToSpeech: "Here is the products list"
                      }
                    },
                    {
                      carouselBrowse: {
                        items: elementsVal
                     },
                    }
                  ],
                 }
              };



              }

          //   for (var val in bodyData.products) {
          //     // console.log("Name is", bodyData.products[val].Name);
          //     elementsVal.push({
          //         title: bodyData.products[val].Name,
          //         openUrlAction: {
          //           url: bodyData.products[val].imageURL
          //         },
          //         description: bodyData.products[val].description +'Variant:'+ bodyData.products[val].variant +'Price:'+ bodyData.products[val].price,
          //         footer: bodyData.products[val].Name,
          //         image: {
          //           url: bodyData.products[val].imageURL,
          //           accessibilityText: bodyData.products[val].Name
          //         }
          //     })
          //  }


    
          //  const googlePayload = {
          //       expectUserResponse: true,
          //       richResponse: {
          //         items: [
          //           {
          //             simpleResponse: {
          //               textToSpeech: "Here is the products list"
          //             }
          //           },
          //           {
          //             carouselBrowse: {
          //               items: elementsVal
          //            },
          //           }
          //         ],
          //        }
          //  };
      
            // console.log("elements val googlePayload", JSON.stringify(googlePayload));
            let gpayload = new Payload(agent.ACTIONS_ON_GOOGLE, googlePayload)
            // console.log("gpayload", gpayload);
            output = agent.add(gpayload);
           }
            resolve(output);
        }
      })
    })


  }
            
  


  // console.log("agent resource", agent.requestSource);
  // if(agent.requestSource === agent.ACTIONS_ON_GOOGLE) {
  //     // agent.add(card); 
  //     console.log("agent resource google", agent.ACTIONS_ON_GOOGLE);
  // }

  let intentMap = new Map();

  switch(actionName) {
    case "input.welcome":
            console.log("switch intent", actionName);
            intentMap.set('Default Welcome Intent', welcome);

            break;

    case "other":
            console.log("Switch intent", actionName);
            intentMap.set('other', other);
          
            break;
    case "welcome.greet":
          console.log("Switch intent", actionName);
          intentMap.set('welcome.greetings', welcomeGreet);
          break;
    case "googleAssistantOther":
            console.log("Switch intent", actionName)
            // if(agent.requestSource === agent.ACTIONS_ON_GOOGLE) {
                intentMap.set('Google Assistant Other', googleAssistantOther);
            // }

            // if(agent.requestSource === agent.FACEBOOK) {
              // console.log("Facebook Request");
            // }

          
          
            // "fulfillmentMessages": [
            //   {
            //     "card": {
            //       "title": "H Logo",
            //       "subtitle": "Elephant packshot",
            //       "imageUri": "https://www.himalayawellness.in/-/media/Project/Himalaya/Himalaya-Wellness/Banner/About-himalaya.jpg?h=330&la=en&w=349&hash=42FD7177B95FE449C148F77B9C887ABB",
            //       "buttons": [
            //         {
            //           "text": "Go to buy"
            //         },
            //         {
            //           "text": "Cancel"
            //         }
            //       ]
            //     },
            //     "platform": "FACEBOOK"
            //   },
            //   {
            //     "text": {
            //       "text": [
            //         ""
            //       ]
            //     }
            //   }
            // ],
            break;
    case "order.status":
          console.log("Switch intent", actionName);
          
          // let intentName = req.body.queryResult.intent.displayName;
          let phoneNum = req.body.queryResult.parameters['phone-number'];
          let orderNum = req.body.queryResult.parameters['order-number'];

          // console.log("phone number is", phoneNum);
          // console.log("order number is", orderNum);
          intentMap.set('order.status', orderstatus);
          



            break;
    case "facebookResponse":
            console.log("Switch intent", actionName);
            if(agent.requestSource === agent.FACEBOOK) {
              console.log("Facebook Request");
              intentMap.set('Facebook Response', facebookResponsev2);
            }
            
            break;
    case "store.locator":
            console.log("Switch intent", actionName)
            let city = req.body.queryResult.parameters.location;
            console.log("City is", city);
            intentMap.set('store.locator', storelocator);
            break;
    
    case "storelocator.storelocator-city":
          console.log("Switch intent", actionName)
          intentMap.set('store.locator - city', storelocatorcity);

          break;
    case "product.search":
            console.log("Switch intent", actionName);
            let product = req.body.queryResult.parameters.product;
            console.log("search product is", product);
            intentMap.set('product.search', productsearch);
            break;
    case "unknown.intent":
            console.log("switch intent 1", actionName);
            // intentMap.set('Default Fallback Intent', inputUnknown);
            intentMap.set('Default Fallback Intent', fallback);
            break;
    default:
    
  }

  intentMap.set('Default Fallback Intent', fallback);

  // if (agent.requestSource === agent.ACTIONS_ON_GOOGLE) {
  //     intentMap.set(null, googleAssistantOther);
  //     console.log("googleAssistantOther 1");
  //   } else {
  //     intentMap.set(null, other);
  //     console.log("other");
  // }
  
  agent.handleRequest(intentMap);
  //aa//

});





// app.post("/echo", function(req, res) {
//   var speech =
//     req.body.result &&
//     req.body.result.parameters &&
//     req.body.result.parameters.echoText
//       ? req.body.result.parameters.echoText
//       : "Seems like some problem. Speak again.";
//   return res.json({
//     speech: speech,
//     displayText: speech,
//     source: "webhook-echo-sample"
//   });
// });




app.listen(port, function() {
  console.log(`Listening on port ${port}`);
});

